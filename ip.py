#!/usr/bin/env python3
from threading import Thread
import numpy as np
from datetime import date, timedelta
import os


def FMA_period():
    for hour in ['00', '12']:
        ip_array = np.zeros((2,13,3,25,49))
        startday, endday = date(2016,1,31), date(2016,4,30)
        day_number = (endday - startday).days
        print(day_number)
        for i in range(day_number):
            results = [None]
            day_ip(startday, hour, results, 0)
            ip_array += results[0]
            startday += timedelta(days=1)        
        np.save('IP-FMA-' + hour, ip_array/day_number) 


def MJJ_period():
    for hour in ['00', '12']:
        ip_array = np.zeros((2,13,3,25,49))
        startday, endday = date(2016,4,30), date(2016,7,31)
        day_number = (endday - startday).days
        print(day_number)
        for i in range(day_number):
            results = [None]
            day_ip(startday, hour, results, 0)
            ip_array += results[0]
            startday += timedelta(days=1)        
        np.save('IP-MJJ-' + hour, ip_array/day_number) 
        

def ASO_period():
    for hour in ['00', '12']:
        ip_array = np.zeros((2,13,3,25,49))
        startday, endday = date(2016,7,31), date(2016,10,31)
        day_number = (endday - startday).days
        print(day_number)
        for i in range(day_number):
            results = [None]
            day_ip(startday, hour, results, 0)
            ip_array += results[0]
            startday += timedelta(days=1)        
        np.save('IP-ASO-' + hour, ip_array/day_number) 


def NDJ_period():
    for hour in ['00', '12']:
        ip_array = np.zeros((2,13,3,25,49))
        startday, endday = date(2015,12,31), date(2016,1,31)
        day_number_1 = (endday - startday).days
        print(day_number_1)
        for i in range(day_number_1):
            results = [None]
            day_ip(startday, hour, results, 0)
            ip_array += results[0]
            startday += timedelta(days=1)        
        startday, endday = date(2016,10,31), date(2016,12,31)
        day_number_2 = (endday - startday).days
        print(day_number_2)
        for i in range(day_number_2):
            results = [None]
            day_ip(startday, hour, results, 0)
            ip_array += results[0]
            startday += timedelta(days=1)        
        np.save('IP-NDJ-' + hour, ip_array/(day_number_1+day_number_2)) 
    

def main():
    for hour in ['00', '12']:
        ip_array = np.zeros((22,22,2,14,3,25,49))
        startday,endday = date(2015,12,31),date(2016,12,30)
        day_number = (endday - startday).days
        print(day_number)
        thread_number = 32
        day = startday
        for block in range(day_number // thread_number +1 ):
            threads = [None] * thread_number
            results = [None] * thread_number
            for  i in range(thread_number):
                threads[i] = Thread(target = day_ip, args=(day, hour, results, i))
                threads[i].start()
                day += timedelta(days=1)
            for  i in range(thread_number):
                threads[i].join() 
            for  i in range(thread_number):
                ip_array += results[i] 
        np.save('IP-' + hour, ip_array/(day_number+1))


def day_ip(day,hour,results,index):
    print(day, hour)
    ip_result = np.zeros((22,22,2,14,3,25,49))
    if date(2015,12,31) <= day <= date(2016,12,30):
        land = np.load('land.npy')
        ocean = np.load('ocean.npy')
        datafile = os.path.join(os.getenv('PWD'),'data',day.strftime('DATA-%Y-%m-%d-' + hour + '.npz')) 

        lat,lon = 180,360
        dtime = 49
        h0 = 6000
        sigma0 = 6e-14
        j0 = 1e-9
        cape_0 = 1/3000     # invert cape_0
        coeff = j0 * h0 / sigma0

        latm = int(lat/2)
        ds = (np.ones((lat,lon)))/(2*lon)
        ind = np.arange(latm)
        square = np.sin((np.pi/lat)*(ind+1)) - np.sin((np.pi/lat)*(ind))
        for i in range(lon):
            ds[:latm,i] *= square[::-1]
            ds[latm:,i] *= square

        r_pw    = np.zeros((14,dtime,lat,lon))
        h_coeff = np.ones((2,dtime,lat,lon))
        terr    = np.ones((3,dtime,lat,lon))
        r_up    = np.ones((22,dtime,lat,lon))
        r_do    = np.ones((22,dtime,lat,lon))
        r_2h    = np.zeros((dtime,lat,lon))
        
        cape = np.load(datafile)['cape']
        ctop = np.load(datafile)['ctop']
        cbot = np.load(datafile)['cbot']
        rain = np.load(datafile)['rainc']
        prwa = np.load(datafile)['pw']

        r_2h[1:-1,:,:] = rain[2:,:,:] - rain[:-2,:,:]

#        r_pw[1,1:,:,:] = rain[1:,:,:] - rain[:-1,:,:]          # rain per 1 hour  backward
#        r_pw[2,2:,:,:] = rain[2:,:,:] - rain[:-2,:,:]          # rain per 2 hours  backward
#        r_pw[3,3:,:,:] = rain[3:,:,:] - rain[:-3,:,:]          # rain per 3 hours  backward

#        r_pw[4,:-1,:,:] = rain[1:,:,:] - rain[:-1,:,:]          # rain per 1 hour  forward
#        r_pw[5,:-2,:,:] = rain[2:,:,:] - rain[:-2,:,:]          # rain per 2 hours  forward
#        r_pw[6,:-3,:,:] = rain[3:,:,:] - rain[:-3,:,:]          # rain per 3 hours  forward
        r_pw[13,1:-1,:,:] = rain[2:,:,:] - rain[:-2,:,:]        # rain 1 hour backward and 1 hour forward


        r_pw /= prwa

#        r_pw[7,1:,:,:] = rain[1:,:,:] - rain[:-1,:,:]          # rain per 1 hour  backward
#        r_pw[8,2:,:,:] = rain[2:,:,:] - rain[:-2,:,:]          # rain per 2 hours  backward
#        r_pw[9,3:,:,:] = rain[3:,:,:] - rain[:-3,:,:]          # rain per 3 hours  backward

#        r_pw[10,:-1,:,:] = rain[1:,:,:] - rain[:-1,:,:]          # rain per 1 hour  forward
#        r_pw[11,:-2,:,:] = rain[2:,:,:] - rain[:-2,:,:]          # rain per 2 hours  forward
#        r_pw[12,:-3,:,:] = rain[3:,:,:] - rain[:-3,:,:]          # rain per 3 hours  forward
#
#        r_pw[0]  = np.ones((dtime,lat,lon))

        h_coeff[1] = np.exp(-cbot/h0) - np.exp(-ctop/h0)
        terr[1] = land
        terr[2] = ocean    

        for j in range(22):
            r_up[j] = np.heaviside(r_2h - j, 1)
            r_do[j] = np.heaviside(-r_2h + j, 1)
        
        for i_up in range(22):
            for i_do in range(22):
                for i_h in range(1,2):
                    for i_r in range(13,14):
                        for i_t in range(1):

#                            ip_tp = coeff * ds * h_coeff[i_h] * r_pw[i_r] * terr[i_t]
                             ip_tp = coeff * ds * h_coeff[i_h] * r_pw[i_r] * terr[i_t] * r_up[i_up] * r_do[i_do]
#                            ip_tp_cape_sq = ip_tp * cape * cape_0
#                            ip_result[i_up, i_do, i_h, i_r, i_t, 0, :] = ip_tp.sum(axis=(1,2))
#                            ip_result[i_up, i_do, i_h, i_r, i_t, 1, :] = ip_tp_cape_sq.sum(axis=(1,2))
#                            ip_result[i_up, i_do, i_h, i_r, i_t, 2, :] = (ip_tp * np.power(cape * cape_0, 2)).sum(axis=(1,2))
                    
#                            for i in range(5,16):
                             for i in range(10,11):
                                ip_result[i_up, i_do, i_h, i_r, i_t, i-2, :] = (ip_tp * np.heaviside(cape - 100 * i, 1)).sum(axis=(1,2))
#                                ip_result[i_up, i_do, i_h, i_r, i_t, i+9, :] = (ip_tp_cape_sq * np.heaviside(cape - 100 * i, 1)).sum(axis=(1,2))

    results[index] = ip_result


if __name__ == '__main__':
#    FMA_period()
#    MJJ_period()
#    ASO_period()
#    NDJ_period()
    main()

