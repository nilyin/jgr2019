#!/usr/bin/env python3
import numpy as np
from datetime import date, timedelta
from dateutil.rrule import rrule, DAILY
import os


def fmin(array):
    result = (0.5 * array[18,:,:] + array[19:42,:,:].sum(axis=0) + 0.5 * array[42,:,:]) / 24
    return result


periods = []


hours = ['00', '12']
path = os.path.join(os.getenv('PWD'), 'data')  #gdas

startday = date(2015,12,31)
endday = date(2016,12,30)
number = (1 + (endday-startday).days)*len(hours)
print(number)

lat,lon = 180,360
dtime = 49
    
mean_cape = np.zeros((lat,lon))
mean_ctop = np.zeros((lat,lon))
mean_cbot = np.zeros((lat,lon))
mean_rp2h = np.zeros((lat,lon))
mean_pr_w = np.zeros((lat,lon))
mean_prwa = np.zeros((lat,lon))

rp2h_max = 0

for s_hour in hours:    
    for day in rrule(DAILY, dtstart=startday, until=endday):
        print(day)
        day_h = day.strftime('-%Y-%m-%d-' + s_hour + '.npz')

        rp2h = np.zeros((dtime,lat,lon))

        cape = np.load(os.path.join(path, 'DATA' + day_h))['cape']
        ctop = np.load(os.path.join(path, 'DATA' + day_h))['ctop']
        cbot = np.load(os.path.join(path, 'DATA' + day_h))['cbot']
        rain = np.load(os.path.join(path, 'DATA' + day_h))['rainc']
        prwa = np.load(os.path.join(path, 'DATA' + day_h))['pw']

        rp2h[1:-1,:,:] = rain[2:,:,:] - rain[:-2,:,:]         # rain per 2 hours middle
        rp2h_max = max(rp2h_max, np.max(rp2h))
        print(rp2h_max)

        pr_w = rp2h/prwa
        mean_cape += fmin(cape) 
        mean_ctop += fmin(ctop)
        mean_cbot += fmin(cbot)
        mean_prwa += fmin(prwa)     
        mean_rp2h += fmin(rp2h)    
        mean_pr_w += fmin(pr_w)    

#(mean_cape[::-1,:]/number).tofile('cape.bin')               #
#(mean_rp2h[::-1,:]/number).tofile('rp2h.bin')               #   values for NCL graphics
#(mean_pr_w[::-1,:]/number).tofile('pr_w.bin')               #

#np.save('MEANCAPE', mean_cape/number)
#np.save('MEANCTOP', mean_ctop/number)
#np.save('MEANCBOT', mean_cbot/number)
#np.save('MEANPRWA', mean_prwa/number)
#np.save('MEANPR_W', mean_pr_w/number)
#np.save('MEANRP2H', mean_rp2h/number)
