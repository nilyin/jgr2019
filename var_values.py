#!/usr/bin/env python3
import numpy as np
from datetime import date, timedelta
from dateutil.rrule import rrule, DAILY
import os


hours = ['00', '12']
path = os.path.join(os.getenv('PWD'), 'data')  #gdas

startday = date(2015,12,31)
endday = date(2016,12,30)
number = (1 + (endday-startday).days)
print(number)

dtime, lat, lon = 49, 180, 360

for s_hour in hours:    
    
    var_cape = np.zeros((dtime,lat,lon))
    var_rp2h = np.zeros((dtime,lat,lon))
    var_rp1h = np.zeros((dtime,lat,lon))
    var_pr_w = np.zeros((dtime,lat,lon))
    var_prwa = np.zeros((dtime,lat,lon))

    for day in rrule(DAILY, dtstart=startday, until=endday):
        print(day)
        day_h = day.strftime('-%Y-%m-%d-' + s_hour + '.npz')

        rp1h = np.zeros((dtime,lat,lon))
        rp2h = np.zeros((dtime,lat,lon))

        cape = np.load(os.path.join(path, 'DATA' + day_h))['cape']
        rain = np.load(os.path.join(path, 'DATA' + day_h))['rainc']
        prwa = np.load(os.path.join(path, 'DATA' + day_h))['pw']


        rp1h[1:,:,:] = rain[1:,:,:] - rain[:-1,:,:]         # rain per 2 hours middle
        rp2h[1:-1,:,:] = rain[2:,:,:] - rain[:-2,:,:]         # rain per 2 hours middle

        pr_w = rp2h/prwa
        var_cape += cape 
        var_prwa += prwa     
        var_rp2h += rp2h    
        var_rp1h += rp1h    
        var_pr_w += pr_w    

#(mean_cape[::-1,:]/number).tofile('cape.bin')               #
#(mean_rp2h[::-1,:]/number).tofile('rp2h.bin')               #   values for NCL graphics
#(mean_pr_w[::-1,:]/number).tofile('pr_w.bin')               #

    np.save('VARCAPE-' + s_hour, var_cape/number)
    np.save('VARPRWA-' + s_hour, var_prwa/number)
    np.save('VARRP2H-' + s_hour, var_rp2h/number)
    np.save('VARRP1H_FORWARD-' + s_hour, var_rp1h/number)
    np.save('VARPR_W-' + s_hour, var_pr_w/number)
